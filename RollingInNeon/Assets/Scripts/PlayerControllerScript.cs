﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerScript : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] Camera cam = null;
    Vector3 inputForce;

    [SerializeField] private float force = 1000;
    [SerializeField] Transform camPivot = null;
    [SerializeField] float verticalFollowRange = 15f;
    [SerializeField] int MAX_limitFlipTimes = 1;
    int limitFlipTimes = 1;
    float timeOfContact = 1;
    public float Force { get { return force * Time.deltaTime; } }
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        limitFlipTimes = MAX_limitFlipTimes;
        timeOfContact = 1;
    }


    void Update()
    {
        inputForce = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        inputForce = Vector2.ClampMagnitude(inputForce, 1);

        Vector3 camF = cam.transform.forward;
        Vector3 camR = cam.transform.right;
        camF.y = 0;
        camR.y = 0;
        camF = camF.normalized;
        camR = camR.normalized;

        rb.AddForce((camF * inputForce.y + camR * inputForce.x) * Time.deltaTime * force);
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (limitFlipTimes > 0)
            {
                Physics.gravity = new Vector3(Physics.gravity.x, -Physics.gravity.y, Physics.gravity.z);
                limitFlipTimes--;
            }
        }
        CamPivotUpdate();
    }

    void CamPivotUpdate()
    {
        if (Physics.gravity.y > 0)
        {
            if (camPivot.position.y + verticalFollowRange < this.transform.position.y - verticalFollowRange * 0.2f)
            {
                camPivot.position += new Vector3(0, Mathf.Abs(this.transform.position.y - camPivot.position.y) * Time.deltaTime * 0.5f, 0);
            }
            else if (camPivot.position.y + verticalFollowRange >= this.transform.position.y + verticalFollowRange * 0.2f)
            {
                camPivot.position += new Vector3(0, -Mathf.Abs(this.transform.position.y - camPivot.position.y) * Time.deltaTime * 0.5f, 0);
            }
        }
        if (Physics.gravity.y < 0)
        {
            if (camPivot.position.y - verticalFollowRange <= this.transform.position.y - verticalFollowRange * 0.2f)
            {
                camPivot.position += new Vector3(0, Mathf.Abs(this.transform.position.y - camPivot.position.y) * Time.deltaTime * 0.5f, 0);
            }
            else if (camPivot.position.y - verticalFollowRange > this.transform.position.y + verticalFollowRange * 0.2f)
            {
                camPivot.position += new Vector3(0, -Mathf.Abs(this.transform.position.y - camPivot.position.y) * Time.deltaTime * 0.5f, 0);
            }
        }

        camPivot.position = new Vector3(this.transform.position.x, camPivot.position.y, this.transform.position.z);
    }

    private void OnCollisionEnter(Collision other)
    {
        limitFlipTimes = 0;
    }
    private void OnCollisionStay(Collision other)
    {
        if (timeOfContact < 1)
        {
            timeOfContact += Time.deltaTime;
        }
        else
        {
            limitFlipTimes = MAX_limitFlipTimes;
        }
    }
    private void OnCollisionExit(Collision other)
    {
        timeOfContact = 0;
    }
}
