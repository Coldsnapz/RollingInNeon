﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeeThroughDetection : MonoBehaviour {
    public Transform playerTransform = null;
    public Transform CameraTransform = null;
    private SeeThroughObject m_CurrentObject;
    private SeeThroughObject m_LastObject;

    void Start () {
        StartCoroutine (DetectPlayerObstruction ());
    }

    IEnumerator DetectPlayerObstruction () {
        while (true) {
            yield return new WaitForSeconds (0.5f);

            Vector3 dir = (playerTransform.position - CameraTransform.position).normalized;
            RaycastHit hit;

            if (Physics.Raycast (CameraTransform.position, dir, out hit, Mathf.Infinity)) {

                SeeThroughObject _seeThroughObject = hit.collider.gameObject.GetComponent<SeeThroughObject> ();

                if (_seeThroughObject) {
                    iTween.FadeTo (hit.collider.gameObject, .25f, .5f);
                    _seeThroughObject.SetTransparent ();
                    m_CurrentObject = _seeThroughObject;

                    if (m_LastObject != m_CurrentObject) {
                        if (m_LastObject) {
                            iTween.FadeTo (m_LastObject.gameObject, 1, .5f);
                            m_LastObject = null;
                        }
                    }

                } else {
                    if (m_CurrentObject) {
                        iTween.FadeTo (m_CurrentObject.gameObject, 1, .5f);
                        m_CurrentObject = null;
                        m_CurrentObject = null;
                    }
                }
            }
        }
    }
    private void LateUpdate () {
        m_LastObject = m_CurrentObject;
    }
}