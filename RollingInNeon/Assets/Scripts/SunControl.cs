﻿using UnityEngine;
public class SunControl : MonoBehaviour
{
    [SerializeField] private float sunRotationDegreePerSecond = 30.0f;
    private Light sun;
    void Start()
    {
        sun = this.GetComponent<Light>();
    }
    void Update()
    {
        sun.transform.Rotate(new Vector3(0, 1, 0), this.sunRotationDegreePerSecond * Time.deltaTime);
    }
}